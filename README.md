# Refactor!

The following code a simple API, which has the ability to retrieve users from a CSV, however, there are many issues within the codebase.



It's your job to refactor the code as best you see fit!



You are not excepted to add any additional functionality.



The code is based on Symfony so try to use the programming patterns that are common within the Symfony environment.



Use this as a chance to demonstrate your ability to write clean object oriented code and respect the REST API standards, use OOP best practices like dependency inversion, abstraction, OOP design patterns, dependency injection etc.



You can define and add as many classes as you with to the file.
